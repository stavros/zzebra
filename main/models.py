import shortuuid
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models  # noqa


def default_username() -> str:
    return shortuuid.ShortUUID("abcdefghijklmnopqrstuvwxyz").random(10)


class Topic(models.Model):
    name = models.SlugField(max_length=500, unique=True)

    def __str__(self) -> str:
        return self.name


class User(AbstractUser):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        "username",
        max_length=150,
        unique=True,
        help_text="Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.",
        validators=[username_validator],
        error_messages={
            "unique": "A user with that username already exists.",
        },
        default=default_username,
    )
    telegram_chat_id = models.CharField(
        max_length=1000, blank=True, null=True, unique=True
    )

    topics = models.ManyToManyField(Topic)

    def __str__(self) -> str:
        return self.username
