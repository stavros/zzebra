import json
from pprint import pprint

from django.conf import settings
from django.http import Http404
from django.http import HttpResponse

from .models import User
from .utils import send_telegram_message


def telegram(request):
    try:
        body = json.loads(request.body)
    except Exception:
        raise Http404

    if "message" not in body:
        return HttpResponse("OK")

    if settings.DEBUG:
        pprint(body)

    chat_id = body["message"]["chat"]["id"]
    text = body["message"].get("text", "")

    user, created = User.objects.get_or_create(telegram_chat_id=chat_id)
    if created:
        send_telegram_message(chat_id, f"Created! {text}")
    else:
        send_telegram_message(chat_id, "Thanks!")

    return HttpResponse("OK")
