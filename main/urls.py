"""The main application's URLs."""
from django.urls import path

from . import views
from . import views_webhooks

app_name = "main"
urlpatterns = [
    path("webhooks/telegram/", views_webhooks.telegram, name="telegram"),
    path("", views.index, name="index"),
]
