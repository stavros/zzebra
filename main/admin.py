from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from djangoql.admin import DjangoQLSearchMixin

from .models import Topic
from .models import User


@admin.register(Topic)
class TopicAdmin(DjangoQLSearchMixin, admin.ModelAdmin):
    list_display = ["name"]


@admin.register(User)
class MyUserAdmin(DjangoQLSearchMixin, UserAdmin):
    search_fields = ["username"]
    fieldsets = (
        ("Credentials", {"fields": ("username", "email", "password")}),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        (
            "Important dates",
            {"fields": ("last_login", "date_joined")},
        ),
        ("Various", {"fields": ("telegram_chat_id", "topics")}),
    )
    list_display = ("username", "telegram_chat_id")
    list_filter = ("is_staff", "is_superuser", "is_active")
    raw_id_fields = ["topics"]

    def messages(self, obj):
        return obj.emailmessage_set.count()

    def is_subscriber(self, obj):
        return obj.member_status == 1

    is_subscriber.boolean = True  # type: ignore
