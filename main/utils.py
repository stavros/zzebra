import requests
from django.conf import settings


def send_telegram_message(chat_id: str, message: str) -> None:
    """
    Send a message on Telegram to the given chat ID.

    Does not crash, logs to Sentry instead.
    """
    try:
        requests.post(
            f"https://api.telegram.org/bot{settings.TELEGRAM_TOKEN}/sendMessage",
            data={"chat_id": chat_id, "text": message},
        )
    except Exception as e:
        from sentry_sdk import capture_exception

        capture_exception(e)
