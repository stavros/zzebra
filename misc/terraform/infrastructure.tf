terraform {
  backend "http" {
  }
}

terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}


variable "cloudflare_token" {}
variable "zone_id" {
  default = "1771f02ddd360106ef7d80ff03a688b7"
}

variable "ipv6_ip" { default = "2a01:4f8:1c0c:6109::1" }
variable "ipv4_ip" { default = "195.201.40.251" }
variable "domain" { default = "zzebra.org" }

resource "cloudflare_record" "v6_root" {
  zone_id = var.zone_id
  type    = "AAAA"
  name    = "@"
  proxied = "true"
  value   = var.ipv6_ip
}

resource "cloudflare_record" "root" {
  name    = "@"
  zone_id = var.zone_id
  type    = "A"
  proxied = "true"
  value   = var.ipv4_ip
}

resource "cloudflare_record" "v6_www" {
  zone_id = var.zone_id
  type    = "AAAA"
  name    = "www"
  proxied = "true"
  value   = var.ipv6_ip
}

resource "cloudflare_record" "www" {
  name    = "www"
  zone_id = var.zone_id
  type    = "A"
  proxied = "true"
  value   = var.ipv4_ip
}

resource "cloudflare_record" "dmarc" {
  zone_id = var.zone_id
  name    = "_dmarc"
  type    = "TXT"
  value   = "v=DMARC1;p=none;pct=20;aspf=r;adkim=r;"
}
