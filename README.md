Zzebra
======

Remember [Aardvark](https://en.wikipedia.org/wiki/Aardvark_(search_engine))?
Of course you don't, nobody does.
Know why?
Because they were doing so well that Google bought them and shut them down, to prevent any other search engine from outgoogling them.

Zzebra aims to take that good idea and implement it again, because Google (and all other search engines) are terrible and nobody can find anything interesting any more.

Also, if Google offers to buy us for a bunch of millions, we'll probably[^1] say no.

So, use Zzebra and be part of the new search revolution:
Not searching at all, but just talking to each other instead!

Zzebra: Because who cares.

[^1]: Not.
